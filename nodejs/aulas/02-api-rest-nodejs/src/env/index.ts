import { config } from 'dotenv'
import { z } from 'zod'

switch (process.env.NODE_ENV) {
  case 'test':
    config({ path: '.env.test' })
    break
  default:
    config({ path: '.env' })
    break
}

const envSchema = z.object({
  NODE_ENV: z
    .enum(['development', 'test', 'production'])
    .default('development'),
  DATABASE_CLIENT: z.enum(['sqlite', 'pg']),
  DATABASE_URL: z.string(),
  PORT: z.coerce.number().default(3333),
})

export const env = envSchema.parse(process.env)
