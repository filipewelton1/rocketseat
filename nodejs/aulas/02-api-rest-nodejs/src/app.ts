import fastify from 'fastify'
import cookie from '@fastify/cookie'

import { transactionRoutes } from './routes/transactions'
import { checkSessionId } from './middlewares/session-id'

const app = fastify()

app.register(cookie)
app.addHook('preHandler', checkSessionId)
app.register(transactionRoutes, { prefix: 'transactions' })

export { app }
