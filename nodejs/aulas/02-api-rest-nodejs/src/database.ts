import { knex as setupKnex, Knex } from 'knex'

import { env } from './env'

if (!process.env.DATABASE_URL) {
  throw new Error('DATABASE_URL is undefined.')
}

export const config: Knex.Config = {
  client: 'sqlite3',
  connection:
    env.DATABASE_CLIENT === 'sqlite'
      ? { filename: env.DATABASE_URL }
      : env.DATABASE_URL,
  useNullAsDefault: false,
  migrations: {
    extension: 'ts',
    directory: './db/migrations',
  },
}

export const knex = setupKnex(config)
