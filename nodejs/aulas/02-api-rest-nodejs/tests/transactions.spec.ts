import { afterAll, beforeAll, it, describe, expect, beforeEach } from 'vitest'
import supertest from 'supertest'
import { execSync } from 'node:child_process'

import { app } from '../src/app'

describe('Transaction routes', () => {
  beforeAll(async () => {
    await app.ready()
  })

  beforeEach(async () => {
    execSync('npm run knex -- migrate:latest')
  })

  afterAll(async () => {
    await app.close()
  })

  it('should be able to create a new transaction', async () => {
    await supertest(app.server)
      .post('/transactions')
      .send({
        title: 'Freelance',
        amount: 1000,
        type: 'credit',
      })
      .expect(201)
  })

  it('should be able to list all transactions', async () => {
    const creationResponse = await supertest(app.server)
      .post('/transactions')
      .send({
        title: 'Freelance',
        amount: 1000,
        type: 'credit',
      })

    const cookies = creationResponse.get('Set-Cookie') || []

    const transactionList = await supertest(app.server)
      .get('/transactions')
      .set('Cookie', cookies)
      .expect(200)

    expect(transactionList.body.transactions).toEqual([
      expect.objectContaining({
        title: 'Freelance',
        amount: 1000,
      }),
    ])
  })

  it('should be able to get a specif transaction', async () => {
    const transactionCreationResponse = await supertest(app.server)
      .post('/transactions')
      .send({
        title: 'Freelance',
        amount: 1000,
        type: 'credit',
      })

    const cookies = transactionCreationResponse.get('Set-Cookie') || []

    const transactionList = await supertest(app.server)
      .get('/transactions')
      .set('Cookie', cookies)
      .expect(200)

    const transactionId = transactionList.body.transactions[0].id

    const transactionResponse = await supertest(app.server)
      .get(`/transactions/${transactionId}`)
      .set('Cookie', cookies)
      .expect(200)

    expect(transactionResponse.body.transaction).toEqual(
      expect.objectContaining({
        title: 'Freelance',
        amount: 1000,
      }),
    )
  })

  it('should be able to get the summary', async () => {
    const response = await supertest(app.server).post('/transactions').send({
      title: 'Freelance',
      amount: 10000,
      type: 'credit',
    })

    const cookies = response.get('Set-Cookie') || []

    await supertest(app.server)
      .post('/transactions')
      .set('Cookie', cookies)
      .send({
        title: 'Market',
        amount: 3000,
        type: 'debit',
      })

    const summary = await supertest(app.server)
      .get('/transactions/summary')
      .set('Cookie', cookies)
      .expect(200)

    expect(summary.body).toEqual(
      expect.objectContaining({
        amount: 7000,
      }),
    )
  })
})
