import fs from 'node:fs'
import { Writable } from 'node:stream'
import { Parser } from 'csv-parse'

class TaskWriter extends Writable {
  _write(chunk, _encoding, callback) {
    fetch('http://localhost:3000/tasks', {
      method: 'POST',
      body: chunk
    })
      .finally(() => callback())
  }
}

const taskWriter = new TaskWriter()

const result = fs.createReadStream('tasks.csv')
  .pipe(new Parser())
  .on('data', ([title, description]) => {
    if (title == 'title' && description == 'description') return

    const buf = Buffer.from(JSON.stringify({ title, description }))
    taskWriter.write(buf)
  })
