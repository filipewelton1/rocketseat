import { randomUUID } from 'node:crypto'
import fs from 'node:fs/promises'

const url = new URL('db.json', import.meta.url)

export class Database {
  #database = {}

  constructor() {
    fs.readFile(url, 'utf-8')
      .then((data) => {
        this.#database = JSON.parse(data)
      })
      .catch(() => this.#persist())
  }

  #persist() {
    fs.writeFile(url, JSON.stringify(this.#database))
  }

  insert(data) {
    const id = randomUUID()

    this.#database[id] = {
      ...data, id,
      created_at: Date.now(),
      completed_at: null,
      updated_at: null
    }

    this.#persist()

    return data
  }

  find({ title, description }) {
    let data = this.#database ?? {}

    data = Object
      .values(data)
      .filter((task) => {
        let result = false

        if (!!title) {
          result = task.title.toLowerCase().includes(title.toLowerCase())
        }

        if (!!description) {
          result = task.description.toLowerCase().includes(description.toLowerCase())
        }

        return result
      })

    return data
  }

  update(id, data) {
    const currentData = this.#database[id]

    if (!currentData) {
      throw new Error('Task not found.')
    }

    const updatedData = {
      ...currentData,
      title: data.title ?? currentData.title,
      description: data.description ?? currentData.description,
      updated_at: Date.now()
    }

    this.#database[id] = updatedData
    this.#persist()

    return updatedData
  }

  delete(id) {
    const task = this.#database[id]

    if (!task) {
      throw new Error('Task not found.')
    }

    this.#database[id] = undefined
    this.#persist()
  }

  completeTask(id) {
    let task = this.#database[id]

    if (!task) {
      throw new Error('Task not found.')
    }

    if (!task.completed_at) {
      task.completed_at = Date.now()
    } else {
      task.completed_at = null
    }

    this.#database[id] = task
    this.#persist()
  }
}
