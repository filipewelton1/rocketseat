import { buildRoutePath } from './utils/build-route-path.js'
import { Database } from './database.js'

const db = new Database()

export const routes = [
  {
    method: 'POST',
    path: buildRoutePath('/tasks'),
    handler: (req, res) => {
      const { title, description } = req.body

      db.insert({ title, description })
      res.writeHead(201).end()
    }
  },
  {
    method: 'GET',
    path: buildRoutePath('/tasks'),
    handler: (req, res) => {
      const { query } = req
      const tasks = db.find(query)

      return res
        .setHeader('Content-Type', 'application/json')
        .end(JSON.stringify(tasks))
    }
  },
  {
    method: 'PUT',
    path: buildRoutePath('/tasks/:id'),
    handler: (req, res) => {
      try {
        const { id } = req.params
        const taskUpdated = db.update(id, req.body)

        res
          .setHeader('Content-Type', 'application/json')
          .end(JSON.stringify(taskUpdated))
      } catch (error) {
        res.writeHead(404).end(error.message)
      }
    }
  },
  {
    method: 'DELETE',
    path: buildRoutePath('/tasks/:id'),
    handler: (req, res) => {
      const { id } = req.params

      try {
        db.delete(id)
        res.writeHead(204).end()
      } catch (error) {
        res.writeHead(404).end(error.message)
      }
    }
  },
  {
    method: 'PATCH',
    path: buildRoutePath('/tasks/:id/complete'),
    handler: (req, res) => {
      const { id } = req.params

      try {
        db.completeTask(id)
        res.end()
      } catch (error) {
        res.writeHead(404).end(error.message)
      }
    }
  }
]
